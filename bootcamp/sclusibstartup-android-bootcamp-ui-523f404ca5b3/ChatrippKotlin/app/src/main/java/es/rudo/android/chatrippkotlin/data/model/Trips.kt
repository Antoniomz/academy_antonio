package es.rudo.android.chatrippkotlin.data.model

import java.io.Serializable

class Trips : Serializable {

    var id: String? = null
    var end_date: String? = null
    var start_date: String? = null
    var destination: Destination? = null

}