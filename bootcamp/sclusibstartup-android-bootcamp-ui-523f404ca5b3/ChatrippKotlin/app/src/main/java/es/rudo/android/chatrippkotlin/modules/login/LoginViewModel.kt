package es.rudo.android.chatrippkotlin.modules.login


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.chatrippkotlin.App
import es.rudo.android.chatrippkotlin.api.Config
import es.rudo.android.chatrippkotlin.api.RetrofitClient
import es.rudo.android.chatrippkotlin.data.model.Login
import es.rudo.android.chatrippkotlin.data.model.Profile
import es.rudo.android.chatrippkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class LoginViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    var showError = MutableLiveData<String>()

    var _eventLoginCorrect = MutableLiveData<Boolean>()
    var goRegister = MutableLiveData<Boolean>()

    fun postLogin() {
        val login = Login()

        login.client_id = Config.CLIENT_ID
        login.client_secret = Config.CLIENT_SECRET
        login.grant_type = Config.GRANT_TYPE_LOGIN
        login.username = username.value
        login.password = password.value

        CoroutineScope(Dispatchers.IO).launch {
            //val responseLogin: Response<Login> = retrofitClient.postLogin(login)
            retrofitClient.apiCall({
                retrofitClient.postLogin(login)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseLogin: Login = response.body() as Login
                            App.preferences.setAccessToken(responseLogin.access_token)
                            App.preferences.setRefreshToken(responseLogin.refresh_token)
                            getMe()
                        } else {
                            showError.value = "Error creating intent session"
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                        showError.value = "Network error"
                    }
                })
        }
    }

    fun getMe() {

        CoroutineScope(Dispatchers.IO).launch {
            //val responseMe = retrofitClient.getMe()
            RetrofitClient().apiCall({
                RetrofitClient().getMe()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseMe: Profile = response.body() as Profile
                            App.preferences.setUserId(responseMe.id)
                            _eventLoginCorrect.value = true
                        }
                    }
                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }

    }

    fun checkLogin() {
        postLogin()
    }

    fun goRegister(){
        goRegister.value = true
    }
}