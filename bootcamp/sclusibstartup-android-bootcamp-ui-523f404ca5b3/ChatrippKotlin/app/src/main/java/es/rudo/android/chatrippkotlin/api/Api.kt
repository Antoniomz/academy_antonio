package es.rudo.android.chatrippkotlin.api

import android.database.Observable
import es.rudo.android.chatrippkotlin.data.model.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface Api {

    @GET("/rates")
    fun getRates(): Observable<List<String>>

    //LOGIN
    @POST("auth/token/")
    suspend fun postLogin(@Body login: Login): Response<Login>

    //GET ME
    @GET("users/me/")
    suspend fun getMe(): Response<Profile>

    //GET CITIES
    @GET("cities/")
    suspend fun getCities(): Response<Pager<City>>

    @POST("auth/register/")
    suspend fun postRegister(@Body profile: Profile): Response<Profile>

    @GET("chats/")
    suspend fun getPosts(): Response<Posts>

    @GET("trips/")

    suspend fun getTrips(): Response<Trips>

}