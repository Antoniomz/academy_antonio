package es.rudo.android.chatrippkotlin.data.model

class Destination {

    var id: String? = null
    var name: String? = null
    var country: Country? = null
    var medias: List<Media?>? = null

}