package es.rudo.android.chatrippkotlin.data.model

class Media {
    var file: String? = null
    var thumbnail: String? = null
    var midsize: String? = null
    var fullsize: String? = null
    var created_at: String? = null
}