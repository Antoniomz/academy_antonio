package es.rudo.android.chatrippkotlin.modules.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.rudo.android.chatrippkotlin.R
import es.rudo.android.chatrippkotlin.databinding.ActivityLoginBinding
import es.rudo.android.chatrippkotlin.databinding.ActivityRegisterBinding
import es.rudo.android.chatrippkotlin.modules.exercise.ExerciseActivity
import es.rudo.android.chatrippkotlin.modules.login.LoginViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)

        binding.lifecycleOwner = this
        binding.registerViewModel = viewModel

        initObservers()
    }

    fun initObservers(){

        viewModel.usernameError.observe(this, Observer<String>{
            input_username.error = it
        })

        viewModel.emailError.observe(this, Observer<String>{
            input_email.error = it
        })

        viewModel.passwordError.observe(this, Observer<String>{
            input_password.error = it
        })

        viewModel.confirmPasswordError.observe(this, Observer<String>{
            input_confirmpassword.error = it
        })

        viewModel.termsError.observe(this, Observer<Boolean>{
            check_terms.error = it.toString()
        })

        viewModel.isCorrect.observe(this, Observer<Boolean> { isSuccessful ->
            if (isSuccessful){
                openActivity()
            }
        })

    }

    fun openActivity(){
        intent = Intent(applicationContext, ExerciseActivity::class.java)
        startActivity(intent)
    }
}