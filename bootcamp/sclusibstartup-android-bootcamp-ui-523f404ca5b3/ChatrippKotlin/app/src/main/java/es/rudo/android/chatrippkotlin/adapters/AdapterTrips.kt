package es.rudo.android.chatrippkotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import es.rudo.android.chatrippkotlin.R
import es.rudo.android.chatrippkotlin.data.model.Trips
import kotlinx.android.synthetic.main.item_cardtrip.view.*

class AdapterTrips(val list: List<Trips>): RecyclerView.Adapter<AdapterTrips.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterTrips.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_cardtrip, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AdapterTrips.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(val v: View): RecyclerView.ViewHolder(v){

        fun bind(trips: Trips){
            v.txt_country.text = trips.destination?.country?.name
            v.txt_trip.text = trips.destination?.name
            v.txt_first_date.text = trips.start_date
            v.txt_finish_date.text = trips.end_date
        }

    }
}