package es.rudo.android.chatrippkotlin.modules.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.rudo.android.chatrippkotlin.R
import es.rudo.android.chatrippkotlin.databinding.ActivityLoginBinding
import es.rudo.android.chatrippkotlin.modules.exercise.ExerciseActivity
import es.rudo.android.chatrippkotlin.modules.register.RegisterActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel

        initObservers()
    }

    fun initObservers(){

        viewModel._eventLoginCorrect.observe(this, Observer<Boolean> { isSuccessful ->
            if (isSuccessful){
                openActivity()
            }
        })

        viewModel.goRegister.observe(this, Observer<Boolean>{
            if (it){
                openRegister()
            }
        })
    }


    fun openActivity(){
        intent = Intent(applicationContext, ExerciseActivity::class.java)
        startActivity(intent)
    }

    fun openRegister(){
        intent = Intent(applicationContext, RegisterActivity::class.java)
        startActivity(intent)
    }
}