package es.rudo.android.chatrippkotlin.api

object Config {
    const val API_URL = "https://chatripp-staging.rudo.es/"
    const val HTTP_CLIENT_AUTHORIZATION = "Bearer "
    const val TYPE_ITEM_AUTHORIZATION = "Authorization"
    const val TYPE_ITEM_WS_AUTH = "ws_auth"
    const val GRANT_TYPE_LOGIN = "password"
    const val GRANT_TYPE = "refresh_token"
    const val CLIENT_ID = "GW9FA88G0xSwYDVkTC9YESzqJYMwLjgiFFExV2Ar"
    const val CLIENT_SECRET = "Ezn0WNQTVIGnxLXAF9Leu8BnkbVYO3wI5bsKrwDJeLZveHnoLDhRRCgDGhLwkVndOJ2SNeXTIYIlDWoOknfnfINrouj3itgATZTX1lCobXewWDHCm4kskGIkRi7xVSJy"
    const val API_URL_STRIPE = "http://translator.rudo.es/"
}