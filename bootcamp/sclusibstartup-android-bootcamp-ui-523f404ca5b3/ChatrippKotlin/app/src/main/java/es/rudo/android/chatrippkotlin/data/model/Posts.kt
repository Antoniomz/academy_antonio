package es.rudo.android.chatrippkotlin.data.model

import java.io.Serializable
import java.util.*

class Posts : Serializable {

    var count: String? = null
    var next: String? = null
    var previous: String? = null
    var results: ArrayList<Posts?>? = null

}