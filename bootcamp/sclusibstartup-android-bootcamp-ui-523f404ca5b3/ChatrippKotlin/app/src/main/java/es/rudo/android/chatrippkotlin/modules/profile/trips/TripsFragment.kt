package es.rudo.android.chatrippkotlin.modules.profile.trips

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import es.rudo.android.chatrippkotlin.R
import es.rudo.android.chatrippkotlin.adapters.AdapterTrips
import es.rudo.android.chatrippkotlin.data.model.Trips
import es.rudo.android.chatrippkotlin.databinding.FragmentProfileBinding
import es.rudo.android.chatrippkotlin.databinding.FragmentTripsBinding
import es.rudo.android.chatrippkotlin.modules.profile.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_profile.*

class TripsFragment : Fragment() {

    private lateinit var binding: FragmentTripsBinding
    private lateinit var viewModel: TripsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_trips, container, false)
        viewModel = ViewModelProvider(this).get(TripsViewModel::class.java)

        binding.lifecycleOwner = this
        binding.tripsViewModel = viewModel

        initRecycler()
        return binding.root
    }

    fun initRecycler(){

    }

}