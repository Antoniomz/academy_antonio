package es.rudo.android.chatrippkotlin.modules.profile.trips

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.chatrippkotlin.api.Pager
import es.rudo.android.chatrippkotlin.api.RetrofitClient
import es.rudo.android.chatrippkotlin.data.model.Trips
import es.rudo.android.chatrippkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class TripsViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    val listTrips = MutableLiveData<List<Trips>>()
    val nextpager = MutableLiveData<String>()


    init {
        getTrips()
    }

    fun getTrips(){

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getTrips()
            },
                object : RetrofitClient.RemoteEmitter{
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE){
                            val responseTrips: Pager<Trips> = response.body() as Pager<Trips>
                            nextpager.value = responseTrips.next
                            listTrips.value = responseTrips.results
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
                )
        }
    }

}