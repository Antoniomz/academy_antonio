package es.rudo.android.chatrippkotlin.modules.profile

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.chatrippkotlin.api.RetrofitClient
import es.rudo.android.chatrippkotlin.data.model.Posts
import es.rudo.android.chatrippkotlin.data.model.Profile
import es.rudo.android.chatrippkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ProfileViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    var showError = MutableLiveData<String>()
    val username = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val image = MutableLiveData<String>()
    var tripsCount = MutableLiveData<String>()
    var friendsCount = MutableLiveData<String>()
    var postsCount = MutableLiveData<String>()

    init {
        loggedUser()
        getPosts()
    }

    fun loggedUser() {

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getMe()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseMe: Profile = response.body() as Profile
                            username.value = responseMe.first_name.toString()
                            description.value = responseMe.bio.toString()
                            image.value = responseMe.image_medias?.file
                            tripsCount.value = responseMe.user_trips?.size.toString()
                            friendsCount.value = responseMe.friends_count?.toString()
                        } else {
                            showError.value = "Invalid user!"
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                        showError.value = "Invalid user!"
                    }

                }
            )
        }

    }

    fun getPosts() {

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getPosts()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE){
                            val responsePosts: Posts = response.body() as Posts
                            postsCount.value = responsePosts.count.toString()
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
            )
        }

    }

}