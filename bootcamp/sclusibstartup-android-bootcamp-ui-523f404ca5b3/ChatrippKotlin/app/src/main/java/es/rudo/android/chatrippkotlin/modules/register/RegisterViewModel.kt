package es.rudo.android.chatrippkotlin.modules.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.chatrippkotlin.api.RetrofitClient
import es.rudo.android.chatrippkotlin.data.model.Profile
import es.rudo.android.chatrippkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class RegisterViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    var showError = MutableLiveData<String>()

    val username = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()
    val terms = MutableLiveData<Boolean>()

    val emailError = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val confirmPasswordError = MutableLiveData<String>()
    val termsError = MutableLiveData<Boolean>()

    var isCorrect = MutableLiveData<Boolean>()
    var isError = MutableLiveData<Boolean>()

    fun postRegister() {

        val profile = Profile()

        profile.username = username.value
        profile.email = email.value
        profile.password = password.value
        profile.password_confirm = confirmPassword.value

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.postRegister(profile)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_CREATED_CODE) {
                            isCorrect.value = true
                        } else {
                            showError.value = "Error creating intent session"
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                        showError.value = "Invalid Register!"
                    }
                }
            )
        }

    }

    private fun checkUsername() {
        if (username.value.isNullOrEmpty()) {
            usernameError.value = "Campos vacíos"
            isError.value = true
        } else isError.value = false
        username.value?.let {
            if (it.length < Constants.USERNAME_LENGTH) {
                usernameError.value = "Al menos dos caracteres"
                isError.value = true
            } else{
                isError.value = false
                usernameError.value = ""
            }
        }
    }

    private fun checkEmail() {
        if (email.value.isNullOrEmpty()) {
            emailError.value = "Campos vacíos"
            isError.value = true
        } else isError.value = false
        email.value?.let {
            if (!it.matches(Constants.EMAIL_PATTERN.toRegex())) {
                emailError.value = "Formato incorrecto"
                isError.value = true
            } else{
                isError.value = false
                emailError.value = ""
            }
        }
    }

    private fun checkPass() {
        if (password.value.isNullOrEmpty()) {
            passwordError.value = "Campos vacios"
            isError.value = true
        } else isError.value = false
        password.value?.let {
            if (!it.matches(Constants.PASSWORD_PATTERN.toRegex())) {
                passwordError.value =
                    "Tiene que contener mayus número y minuscula"
                isError.value = true
            } else isError.value = false
            if (it.length < Constants.PASSWORD_LENGTH) {
                passwordError.value = "Al menos 8 carácteres"
                isError.value = true
            } else{
                isError.value = false
                passwordError.value = ""
            }
        }
    }

    private fun checkConfirm() {
        if (confirmPassword.value.isNullOrEmpty()) {
            confirmPasswordError.value = "Campos vacíos"
            isError.value = true
        } else isError.value = false
        confirmPassword.value?.let {
            if (it != password.value) {
                confirmPasswordError.value = "No coincide"
                isError.value = true
            } else{
                isError.value = false
                confirmPasswordError.value = ""
            }
        }
    }

    private fun checkTerms() {
        if (terms.value != true) {
            isError.value = true
        } else isError.value = false
    }

    fun checkRegister() {
        checkUsername()
        checkEmail()
        checkPass()
        checkConfirm()
        checkTerms()
        if (isError.value != true) {
            postRegister()
        }
    }
}