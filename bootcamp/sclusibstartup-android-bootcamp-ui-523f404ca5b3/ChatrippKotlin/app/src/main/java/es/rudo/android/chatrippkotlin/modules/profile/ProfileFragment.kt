package es.rudo.android.chatrippkotlin.modules.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import es.rudo.android.chatrippkotlin.R
import es.rudo.android.chatrippkotlin.adapters.TabAdapter
import es.rudo.android.chatrippkotlin.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    private lateinit var adapter: TabAdapter
    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this
        binding.profileViewModel = viewModel

        this.adapter = TabAdapter(this)
        binding.fragmentContainer.adapter = adapter

        initObservers()

        return binding.root
    }

    fun initObservers(){
        viewModel.image.observe(viewLifecycleOwner, Observer<String>{
            Picasso.get().load(it).into(image_profile)
        })

        viewModel.tripsCount.observe(viewLifecycleOwner, Observer{
            it?.let {
                setCustomTab(
                    binding.tabLayout, "TRIPS", it, 1
                )
            }
        })

        viewModel.friendsCount.observe(viewLifecycleOwner, Observer{
            it?.let {
                setCustomTab(
                    binding.tabLayout, "FRIENDS", it, 2
                )
            }
        })

        viewModel.postsCount.observe(viewLifecycleOwner, Observer{
            it?.let {
                setCustomTab(
                    binding.tabLayout, "POSTS", it, 0
                )
            }
        })
    }


    fun tabLayout(){
        TabLayoutMediator(tab_layout, fragment_container, TabLayoutMediator.TabConfigurationStrategy { tab, position ->
            when (position) {
                0 -> tab.text = "Opción 1"
                1 -> tab.text = "Opción 2"
                2 -> tab.text = "Opción 3"
            }
        }).attach()
    }

    private fun setCustomTab(tabLayout: TabLayout, tabName: String, tabCount: String, pos: Int) {
        val myTab = LayoutInflater.from(context).inflate(R.layout.custom_tab, null) as LinearLayout
        val textViewName = myTab.findViewById<TextView>(R.id.tab_posts)
        val textViewCount = myTab.findViewById<TextView>(R.id.tab_count)
        textViewCount.text = tabCount
        textViewName.text = tabName
        tabLayout.getTabAt(pos)?.customView = myTab
    }

}