package es.rudo.android.chatrippkotlin.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import es.rudo.android.chatrippkotlin.modules.profile.ProfileFragment
import es.rudo.android.chatrippkotlin.modules.profile.friends.FriendsFragment
import es.rudo.android.chatrippkotlin.modules.profile.posts.PostsFragment
import es.rudo.android.chatrippkotlin.modules.profile.trips.TripsFragment

class TabAdapter(fm: ProfileFragment) : FragmentStateAdapter(fm) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {

        return when (position) {
            1 -> return TripsFragment()
            2 -> return FriendsFragment()
            3 -> return PostsFragment()
            else -> TripsFragment()
        }

    }


}