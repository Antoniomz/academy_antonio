package es.rudo.android.bootcampkotlin.modules.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.data.model.User
import es.rudo.android.bootcampkotlin.databinding.FragmentSearchBinding
import es.rudo.android.bootcampkotlin.modules.home.HomeFragment

class SearchFragment : Fragment() {

    companion object{
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: SearchViewModel
    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)

        binding.lifecycleOwner = this
        binding.searchViewModel = viewModel

        initRecycler()
        return binding.root
    }

    fun initRecycler(){
        binding.rwSearch.layoutManager = LinearLayoutManager(context)
        var users: List<User>
        users = viewModel.users
        viewModel.listUsers.value = users
        val adapter = SearchAdapter(users)
        binding.rwSearch.adapter = adapter
    }
}