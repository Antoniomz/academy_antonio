package es.rudo.android.bootcampkotlin.modules.maps

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.FragmentMapsBinding
import java.io.IOException
import java.util.*

class MapsFragment : Fragment() {

    private lateinit var binding: FragmentMapsBinding
    private lateinit var viewModel: MapsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false)
        viewModel = ViewModelProvider(this).get(MapsViewModel::class.java)

        val mapFragment = childFragmentManager.findFragmentById(R.id.frame_maps) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        return binding.root
    }

    var gMap: GoogleMap? = null

    private val callback = OnMapReadyCallback { googleMap ->

            gMap = googleMap
            val geocoder = Geocoder(context, Locale.getDefault())
            val spain = LatLng(39.0, 0.0)
            googleMap.addMarker(MarkerOptions().position(spain).title("Marker in Spain"))
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(spain))
            gMap?.setOnMapClickListener { latLng ->
                var strAdd = ""
                //Create Marker
                val markerOptions = MarkerOptions()

                //Set marker position
                markerOptions.position(latLng)
                try {
                    val addresses =
                        geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                    if (addresses != null) {
                        val returnAddress = addresses[0]
                        val stringBuilder = StringBuilder("")
                        for (i in 0..returnAddress.maxAddressLineIndex) {
                            stringBuilder.append(returnAddress.getAddressLine(i)).append("\n")
                            strAdd = stringBuilder.toString()
                            val toast =
                                Toast.makeText(context, strAdd, Toast.LENGTH_SHORT)
                            toast.show()
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                //Set latitude and longitude
                markerOptions.title("Latitude: " + latLng.latitude + " Longitude: " + latLng.longitude)
                val toast = Toast.makeText(
                    context,
                    "Latitude: " + latLng.latitude + " Longitude: " + latLng.longitude,
                    Toast.LENGTH_SHORT
                )
                toast.show()

                //Clear other mark
                gMap?.clear()

                //Ad marker on map
                gMap?.addMarker(markerOptions)
            }
        }

}