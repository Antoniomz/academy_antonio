package es.rudo.android.bootcampkotlin.modules.register

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.bootcampkotlin.helpers.Constants

class RegisterViewModel : ViewModel() {

    val error = MutableLiveData<Boolean>()
    val username = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val confirmPasswordError = MutableLiveData<String>()
    val terms = MutableLiveData<Boolean>().apply { postValue(false) }


    // Error on register
    var eventRegisterCorrect = MutableLiveData<Boolean>()

    fun checkRegister(){

        checkUsername()
        emailcheck()
        checkPassword()
        checkConfirmPassword()
        checkTerms()

        if (error.value == false) eventRegisterCorrect.value = true
    }

    fun checkUsername(){
        if (username.value.isNullOrEmpty()){
            usernameError.value = "Este campo no puede estar vacío"
            error.value = true
        }else{
            error.value = false
        }
    }

    /*fun checkEmail(){
        val txt_email: String? = email.value
        if (txt_email.isNullOrEmpty()) {
            emailError.value = "Este campo no puede estar vacío"
            error = true
        }else{
            error = false
        }
    }*/

    fun emailcheck(){
        val txt_email: String? = email.value
                txt_email?.let { txt_email.isNullOrEmpty() || txt_email.matches(Constants.EMAIL_PATTERN.toRegex()) }
                ?: run {
                    emailError.value = "Este campo no puede estar vacío"
                    error.value = true
                }
    }


    fun checkPassword(){
        if (password.value.isNullOrEmpty()){
            passwordError.value = "Este campo no puede estar vacío"
            error.value = true
        }else{
            error.value = false
        }
    }

    fun checkConfirmPassword(){
        if (confirmPassword.value.isNullOrEmpty()){
            confirmPasswordError.value = "Este campo no puede estar vacío"
            error.value = true
        }else{
            error.value = false
        }
    }

    fun checkTerms(){
        val term: Boolean? = terms.value
        if (term != true){
            error.value = true
        }
    }
}


