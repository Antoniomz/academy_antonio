package es.rudo.android.bootcampkotlin.modules.login

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import es.rudo.android.bootcampkotlin.MainActivity
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.ActivityLoginBinding
import es.rudo.android.bootcampkotlin.modules.navigation.NavigationActivity
import es.rudo.android.bootcampkotlin.modules.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewmodel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.lifecycleOwner = this
        binding.loginViewModel = viewmodel

        initObservers()
    }

    private fun initObservers(){
        viewmodel.usernameError.observe(this, Observer<String>{
            et_username.error = it
        })

        viewmodel.passwordError.observe(this, Observer<String>{
            et_password.error = it
        })

        viewmodel.eventLoginCorrect.observe(this, Observer<Boolean>{
            if(it){
                openActivity()
            }
        })

        //Method to create account in textView
        viewmodel.eventRegisterPressed.observe(this, Observer<Boolean>{
            if (it){
                openRegister()
            }
        })
    }

    private fun openActivity(){
        val intent = Intent(this, NavigationActivity::class.java)
        startActivity(intent)
    }

    private fun openRegister(){
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

}