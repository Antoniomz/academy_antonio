package es.rudo.android.bootcampkotlin.helpers

object Constants {

    const val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    const val PASSWORD_PATTERN = "^" + "(?=.*[0-9])" + "(?=.*[a-z])" + "(?=.*[A-Z])" + ".{8,20}" + "$"

    const val mypreference = "mypref"
    const val text_preference = "text"
    const val check = "checkbox"
    const val sw = "switch"

}