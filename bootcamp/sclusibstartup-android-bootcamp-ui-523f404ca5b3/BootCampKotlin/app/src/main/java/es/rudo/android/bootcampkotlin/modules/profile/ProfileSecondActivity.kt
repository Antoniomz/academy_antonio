package es.rudo.android.bootcampkotlin.modules.profile

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.ActivitySecondBinding

class ProfileSecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySecondBinding
    private lateinit var viewModel: ProfileSecondViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =  DataBindingUtil.setContentView(this, R.layout.activity_second)
        viewModel = ViewModelProvider(this).get(ProfileSecondViewModel::class.java)

        binding.lifecycleOwner = this
        binding.secondViewModel = viewModel

        val name: String? = intent.getStringExtra("username")
        binding.textName.text = name
    }

}