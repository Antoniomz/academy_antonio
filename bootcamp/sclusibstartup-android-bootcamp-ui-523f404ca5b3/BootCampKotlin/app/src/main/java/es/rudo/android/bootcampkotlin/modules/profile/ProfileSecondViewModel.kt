package es.rudo.android.bootcampkotlin.modules.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileSecondViewModel : ViewModel() {

    val username = MutableLiveData<String>()

}