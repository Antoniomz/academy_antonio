package es.rudo.android.bootcampkotlin.modules.add

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AddViewModel : ViewModel(){

    val checktext = MutableLiveData<String>()
    val checkbox = MutableLiveData<Boolean>()
    val checkswitch = MutableLiveData<Boolean>()

    var eventAllIsCorrect = MutableLiveData<Boolean>()
    var eventClearAll = MutableLiveData<Boolean>()

    fun allCorrect(){
        eventAllIsCorrect.value = true
    }

    fun clearAll(){
        eventClearAll.value = true
    }

}