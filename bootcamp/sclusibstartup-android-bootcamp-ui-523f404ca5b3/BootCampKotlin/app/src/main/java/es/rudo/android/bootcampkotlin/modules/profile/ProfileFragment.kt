package es.rudo.android.bootcampkotlin.modules.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        binding.lifecycleOwner = this
        binding.profileViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    fun initObservers(){
        viewModel.eventLoginCorrect.observe(viewLifecycleOwner, Observer<Boolean>{
            if(it){
                goSecond()
            }
            Toast.makeText(context, "Entrada", Toast.LENGTH_SHORT).show();
        })
    }

    fun goSecond(){
        val value: String = binding.editUsername.text.toString()
        val intent = Intent(context, ProfileSecondActivity::class.java)
        intent.putExtra("username", value)
        startActivity(intent)
    }

}