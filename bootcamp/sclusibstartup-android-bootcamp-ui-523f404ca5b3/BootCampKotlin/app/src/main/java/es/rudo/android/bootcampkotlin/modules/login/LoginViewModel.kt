package es.rudo.android.bootcampkotlin.modules.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.bootcampkotlin.helpers.Constants

class LoginViewModel : ViewModel() {

    private val error = MutableLiveData<Boolean>()
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()

    // Error on login
    var eventLoginCorrect = MutableLiveData<Boolean>()

    // Press create account
    var eventRegisterPressed = MutableLiveData<Boolean>()

    fun checkLogin() {
        val textEmail: String? = username.value

        if (textEmail.isNullOrEmpty()) {
            usernameError.value = "Este campo no puede estar vacío"
            error.value = true
        } else {
            textEmail.let {
                if (it != null) {
                    if (!it.matches(Constants.EMAIL_PATTERN.toRegex())) {
                        usernameError.value = "Formato incorrecto"
                        error.value = true
                    } else {
                        usernameError.value = ""
                        error.value = false
                    }
                }
            }
        }

        if (password.value.isNullOrEmpty()) {
            passwordError.value = "Este campo no puede estar vacío"
            error.value = true
        } else {
            passwordError.value = ""
        }

        if (error.value == false) eventLoginCorrect.value = true
    }

    fun registerPressed() {
        eventRegisterPressed.value = true

    }
}