package es.rudo.android.bootcampkotlin.modules.add

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.FragmentAddBinding
import es.rudo.android.bootcampkotlin.helpers.Constants
import kotlinx.android.synthetic.main.fragment_add.*

class AddFragment : Fragment() {

    private lateinit var binding: FragmentAddBinding
    private lateinit var viewModel: AddViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add, container, false)
        viewModel = ViewModelProvider(this).get(AddViewModel::class.java)

        binding.lifecycleOwner = this
        binding.addViewModel = viewModel

        initObservers()
        get()
        return binding.root
    }

    fun save(){
        val sharedpreferences: SharedPreferences? = context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)

        val n: String = edit_text.text.toString()
        val c: Boolean = check_edit.isChecked
        val s: Boolean = switch_edit.isChecked

        val editor = sharedpreferences?.edit()
        editor?.putString(Constants.text_preference, n)
        editor?.putBoolean(Constants.check, c)
        editor?.putBoolean(Constants.sw, s)
        editor?.apply()
    }

    fun initObservers(){
        viewModel.eventAllIsCorrect.observe(viewLifecycleOwner, Observer<Boolean>{
            if(it){
                save()
            }
        })

        viewModel.eventClearAll.observe(viewLifecycleOwner, Observer<Boolean>{
          if (it){
              clear()
          }
        })
    }

    fun clear(){
        val sharedpreferences: SharedPreferences? = context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)

        val editor = sharedpreferences?.edit()
        editor?.remove(Constants.text_preference)
        editor?.remove(Constants.check)
        editor?.remove(Constants.sw)
        editor?.apply()

        get()
    }

    fun get(){
        val sharedpreferences: SharedPreferences? = context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)

        val texts: String? = sharedpreferences?.getString(Constants.text_preference, null)
        val checked: Boolean? = sharedpreferences?.getBoolean(Constants.check, false)
        val switch: Boolean? = sharedpreferences?.getBoolean(Constants.sw, false)

        viewModel.checktext.value = texts
        viewModel.checkbox.value = checked
        viewModel.checkswitch.value = switch

    }

}
