package es.rudo.android.bootcampkotlin.data.model

import java.io.Serializable

data class User(var name: String?, var photo: Int?): Serializable {
}