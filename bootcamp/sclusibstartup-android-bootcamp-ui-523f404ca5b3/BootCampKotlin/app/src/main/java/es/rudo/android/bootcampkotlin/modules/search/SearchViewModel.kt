package es.rudo.android.bootcampkotlin.modules.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.data.model.User

class SearchViewModel : ViewModel() {

    val listUsers = MutableLiveData<List<User>>()
    val users: List<User>

    init {
        users = listOf(
            User("Alberto", R.drawable.equipo1),
            User("Sara",  R.drawable.equipo2),
            User("Celia@rudo.es",  R.drawable.equipo1),
            User("Juan",  R.drawable.equipo3),
            User("Julio@rudo,es", R.drawable.ic_baseline_person_24),
            User("Laura",  R.drawable.equipo1),
            User("Marcos", R.drawable.ic_baseline_person_24),
            User("Pepe",  R.drawable.equipo2),
            User("Gregorio@gmail.com", R.drawable.ic_baseline_person_24),
            User("Sheila",  R.drawable.equipo3),
        )
    }

}