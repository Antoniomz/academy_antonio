package es.rudo.android.bootcampkotlin.modules.navigation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.ActivityNavigationBinding
import es.rudo.android.bootcampkotlin.modules.add.AddFragment
import es.rudo.android.bootcampkotlin.modules.home.HomeFragment
import es.rudo.android.bootcampkotlin.modules.maps.MapsFragment
import es.rudo.android.bootcampkotlin.modules.profile.ProfileFragment
import es.rudo.android.bootcampkotlin.modules.search.SearchFragment

class NavigationActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_navigation)
        binding.navigationbar.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        var fragment: Fragment? = null
        when (item.itemId) {
            R.id.action_home -> fragment = HomeFragment()
            R.id.action_search -> fragment = SearchFragment()
            R.id.action_profile -> fragment = ProfileFragment()
            R.id.action_add -> fragment = AddFragment()
            R.id.action_maps -> fragment = MapsFragment()
        }
        return loadFragment(fragment)
    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragments_container, fragment)
                    .commit()
            return true
        }
        return false
    }

}