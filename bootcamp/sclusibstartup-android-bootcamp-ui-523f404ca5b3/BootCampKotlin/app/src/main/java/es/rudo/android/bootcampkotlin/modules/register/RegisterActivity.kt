package es.rudo.android.bootcampkotlin.modules.register

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import es.rudo.android.bootcampkotlin.MainActivity
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.databinding.ActivityRegisterBinding
import es.rudo.android.bootcampkotlin.modules.splash.SplashScreenActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)

        binding.lifecycleOwner = this
        binding.registerViewModel = viewModel

        initObservers()
    }

    private fun initObservers(){

        viewModel.usernameError.observe(this, Observer<String>{
            input_username.error = it
        })

        viewModel.emailError.observe(this, Observer<String>{
            input_email.error = it
        })

        viewModel.passwordError.observe(this, Observer<String>{
            input_password.error = it
        })

        viewModel.confirmPasswordError.observe(this, Observer<String>{
            input_confirmpassword.error = it
        })

        viewModel.eventRegisterCorrect.observe(this, Observer<Boolean>{
            if (it){
                openActivity()
            }
        })

    }

    private fun openActivity(){
        val intent = Intent(this, SplashScreenActivity::class.java)
        startActivity(intent)
    }

}