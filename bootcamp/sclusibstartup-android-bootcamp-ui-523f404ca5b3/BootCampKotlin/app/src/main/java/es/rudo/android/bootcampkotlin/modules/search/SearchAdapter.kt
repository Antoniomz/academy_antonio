package es.rudo.android.bootcampkotlin.modules.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import es.rudo.android.bootcampkotlin.R
import es.rudo.android.bootcampkotlin.data.model.User
import kotlinx.android.synthetic.main.item_search_user.view.*

class SearchAdapter(val listUsers: List<User>) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_search_user, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = listUsers.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listUsers[position])
    }

    class ViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(user: User) {
            v.text_username.text = user.name
        }
    }

}