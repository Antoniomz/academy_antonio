package es.rudo.android.bootcampkotlin.modules.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel : ViewModel() {

    val username = MutableLiveData<String>()
    var eventLoginCorrect = MutableLiveData<Boolean>()

    fun eventCorrect(){
        eventLoginCorrect.value = true
    }

}