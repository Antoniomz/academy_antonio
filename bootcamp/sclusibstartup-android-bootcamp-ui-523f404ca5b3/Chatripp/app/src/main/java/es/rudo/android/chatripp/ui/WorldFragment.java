package es.rudo.android.chatripp.ui;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.api.Pager;
import es.rudo.android.chatripp.entities.Cities;
import es.rudo.android.chatripp.utils.AdapterCities;
import es.rudo.android.chatripp.utils.Constants;

public class WorldFragment extends Fragment {

    View v;
    RecyclerView recyclerView;
    NestedScrollView nestedScrollView;
    ProgressBar progressBar;
    List<Cities> citiesArrayList = new ArrayList<>();
    AdapterCities adapterCities;
    int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_world, container, false);

        nestedScrollView = v.findViewById(R.id.nested_view);
        recyclerView = v.findViewById(R.id.recycler_cities);
        progressBar = v.findViewById(R.id.barprogress_circular);
        getData(page);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                        page++;
                        progressBar.setVisibility(View.VISIBLE);
                        getData(page);
                    }else if (scrollY == 0){
                        page--;
                        getData(page);
                    }
            }
        });
        return v;
    }

    public void getData(int page){
        new DataWebService().getCities(page, new DataStrategy.InteractDispatcherPager<Cities>() {
            @Override
            public void response(int code, Pager<Cities> pager) {
                if (code == Constants.SERVER_BADREQUEST_CODE){
                    Toast.makeText(getContext(), "Bad request", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        citiesArrayList.addAll(pager.getResults()); //add all cities to arraylist
                        adapterCities = new AdapterCities(citiesArrayList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapterCities);
                    }catch (NullPointerException e){
                        Toast.makeText(getContext(), "Bad request", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

}