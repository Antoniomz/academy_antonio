package es.rudo.android.chatripp.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.entities.Profile;

public class AdapterRecycler extends RecyclerView.Adapter<AdapterRecycler.ViewHolderExercise05> {

    List<Profile> friendList;

    public AdapterRecycler(List<Profile> friendList){
        this.friendList = friendList;
    }

    @NonNull
    @Override
    public ViewHolderExercise05 onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cardview_cities, null, false);
        return new ViewHolderExercise05(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderExercise05 holder, int position) {
        holder.name.setText(friendList.get(position).getFirst_name() + " " + friendList.get(position).getLast_name());
        Picasso.get().load(friendList.get(position).getImage_medias().getFile()).into(holder.imgprofile);
    }

    @Override
    public int getItemCount() {

        return friendList.size();
    }

    public class ViewHolderExercise05 extends RecyclerView.ViewHolder {

        TextView name;
        ImageView imgprofile;

        public ViewHolderExercise05(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txt_trip);
            imgprofile = itemView.findViewById(R.id.img_trip);
        }
    }
}
