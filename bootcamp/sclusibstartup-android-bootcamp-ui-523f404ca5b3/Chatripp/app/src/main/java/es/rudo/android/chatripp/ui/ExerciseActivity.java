package es.rudo.android.chatripp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.entities.Profile;

public class ExerciseActivity extends AppCompatActivity{

    final Fragment commentsFragment = new CommentsFragment();
    final Fragment likeFragment = new LikeFragment();
    final Fragment worldFragment = new WorldFragment();
    final Fragment profileFragment = new ProfileFragment();
    final Fragment mapsFragment = new MapsFragment();
    final FragmentManager fm = getSupportFragmentManager();
    private Fragment first;
    private int TabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        BottomNavigationView navigation = findViewById(R.id.bottom_menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadData();
        setOnCreateFragment(navigation, TabId);
    }

    private void setOnCreateFragment(BottomNavigationView navigation, int tabId) {
        fm.beginTransaction().add(R.id.fragment_container, mapsFragment, "1").hide(mapsFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, commentsFragment, "2").hide(commentsFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, worldFragment, "3").hide(worldFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, profileFragment, "4").hide(profileFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, likeFragment, "5").hide(likeFragment).commit();
        if (tabId != -1) {
            switch (tabId) {
                case R.id.action_maps:
                    fm.beginTransaction().show(mapsFragment).commit();
                    first = mapsFragment;
                    break;
                case R.id.action_message:
                    fm.beginTransaction().show(commentsFragment).commit();
                    first = commentsFragment;
                    break;
                case R.id.action_world:
                    fm.beginTransaction().show(worldFragment).commit();
                    first = worldFragment;
                    break;
                case R.id.action_profile:
                    fm.beginTransaction().show(profileFragment).commit();
                    first = profileFragment;
                    break;
                case R.id.action_like:
                    fm.beginTransaction().show(likeFragment).commit();
                    first = likeFragment;
                    break;
            }
            navigation.setSelectedItemId(tabId);
        } else {
            fm.beginTransaction().show(profileFragment).commit();
            first = profileFragment;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            TabId = item.getItemId();
            switch (TabId) {
                case R.id.action_maps:
                    fm.beginTransaction().hide(first).show(mapsFragment).commit();
                    first = mapsFragment;
                    break;
                case R.id.action_message:
                    fm.beginTransaction().hide(first).show(commentsFragment).commit();
                    first = commentsFragment;
                    break;
                case R.id.action_world:
                    fm.beginTransaction().hide(first).show(worldFragment).commit();
                    first = worldFragment;
                    break;
                case R.id.action_profile:
                    fm.beginTransaction().hide(first).show(profileFragment).commit();
                    first = profileFragment;
                    break;
                case R.id.action_like:
                    fm.beginTransaction().hide(first).show(likeFragment).commit();
                    first = likeFragment;
                    break;
            }
            return true;
        }
    };

    private void loadData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        TabId = preferences.getInt("tabId", -1);
    }
    private void saveData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tabId", TabId).commit();
    }
    @Override
    protected void onStop() {
        saveData();
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        fm.beginTransaction().hide(first).commitAllowingStateLoss();
        super.onDestroy();
    }
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

}