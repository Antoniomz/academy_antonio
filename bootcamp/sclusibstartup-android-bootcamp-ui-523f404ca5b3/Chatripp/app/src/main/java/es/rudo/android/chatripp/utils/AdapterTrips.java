package es.rudo.android.chatripp.utils;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.Pager;
import es.rudo.android.chatripp.entities.Trips;

public class AdapterTrips extends RecyclerView.Adapter<AdapterTrips.ViewHolder> {

    List<Trips> trips;
    Date date_end, date_start;
    SimpleDateFormat format = new SimpleDateFormat(Constants.SHORT_DATE_PATTERN);

    public AdapterTrips(List<Trips> trips){
        this.trips = trips;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String startdate = trips.get(position).getStart_date();
        String enddate = trips.get(position).getEnd_date();
        try {
            date_start = new SimpleDateFormat(Constants.SERVER_DATE_PATTERN).parse(startdate);
            date_end = new SimpleDateFormat(Constants.SERVER_DATE_PATTERN).parse(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String firstdate = format.format(date_start);
        String lastdate = format.format(date_end);

        holder.txt_country.setText(trips.get(position).getDestination().getCountry().getName());
        holder.txt_city.setText(trips.get(position).getDestination().getName());
        holder.txt_startdate.setText(firstdate);
        holder.txt_finishdate.setText(lastdate);

        //for arrays of media is empty
        try {
            Picasso.get().load(trips.get(position).getDestination().getMedias().get(0).getFullsize()).into(holder.img);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txt_country, txt_city, txt_startdate, txt_finishdate;
        ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cv = itemView.findViewById(R.id.card_trip);
            txt_country = itemView.findViewById(R.id.txt_country);
            img = itemView.findViewById(R.id.img_trip);
            txt_city = itemView.findViewById(R.id.txt_trip);
            txt_startdate = itemView.findViewById(R.id.txt_first_date);
            txt_finishdate = itemView.findViewById(R.id.txt_finish_date);

        }
    }
}
