package es.rudo.android.chatripp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.android.chatripp.App;
import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.Config;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.api.Token;
import es.rudo.android.chatripp.entities.Login;
import es.rudo.android.chatripp.utils.AppPreferences;
import es.rudo.android.chatripp.utils.Constants;

public class LoginActivity extends AppCompatActivity {

    TextView create_account;
    EditText username, password;
    Button loginbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        loginbtn = findViewById(R.id.btn_login);
        create_account = findViewById(R.id.text_create_account);

        initListeners();

        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegister();
            }
        });

    }

    private void initListeners() {
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoginCorrect()) callLogin();
            }
        });
    }

    private boolean isLoginCorrect() {
        boolean isCorrect = true;

        if (username.getText().toString().isEmpty()) {
            isCorrect = false;
        }
        if (password.getText().toString().isEmpty()) {
            isCorrect = false;
        }

        return isCorrect;
    }

    private void callLogin() {
        loginbtn.setEnabled(false);

        Login login = new Login();
        login.setUsername(username.getText().toString());
        login.setPassword(password.getText().toString());
        login.setGrant_type(Config.GRANT_TYPE_LOGIN);

        new DataWebService().postLogin(login, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    Token token = (Token) object;
                    String auth_token = token.getAccess_token();
                    App.preferences.setAccessToken(auth_token);
                    goToMainactivity();
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Login", Toast.LENGTH_LONG).show();
                    loginbtn.setEnabled(true);
                }
            }
        });
    }

    private void goToMainactivity() {
        Intent intent = new Intent(this, ExerciseActivity.class);
        startActivity(intent);
    }

    private void goToRegister(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}