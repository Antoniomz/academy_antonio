package es.rudo.android.chatripp.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import es.rudo.android.chatripp.App;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;

public class AccessTokenAuthenticator implements Authenticator {

    @Nullable
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        final String accessToken = App.preferences.getAccessToken();
        if (!isRequestWithAccessToken(response) || accessToken == null) {
            return null;
        }
        synchronized (this) {
            final String newAccessToken = App.preferences.getAccessToken();
            // Access token is refreshed in another thread.
            if (!accessToken.equals(newAccessToken)) {
                return newRequestWithAccessToken(response.request(), newAccessToken);
            }

            // Need to refresh an access token
            Call<Token> call = new DataWebService().refreshToken();

            retrofit2.Response<Token> res = call.execute();
            Token data = res.body();
            if (data != null) {
                final String updatedAccessToken = data.getAccess_token();
                final String updatedRefreshToken = data.getRefresh_token();
                App.preferences.setAccessToken(updatedAccessToken);
                App.preferences.setRefreshToken(updatedRefreshToken);
                return newRequestWithAccessToken(response.request(), updatedAccessToken);
            } else {
                return newRequestWithAccessToken(response.request(), App.preferences.getAccessToken());
            }
        }
    }


    private boolean isRequestWithAccessToken(@NonNull Response response) {
        String header = response.request().header("Authorization");
        return header != null && header.startsWith("Bearer");
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .build();
    }
}
