package es.rudo.android.chatripp.utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.entities.Cities;
import es.rudo.android.chatripp.ui.WorldFragment;

public class AdapterCities extends RecyclerView.Adapter<AdapterCities.ViewHolder> {

    private List<Cities> citiesArrayList;

    public AdapterCities(List<Cities> citiesArrayList){
        this.citiesArrayList = citiesArrayList;
    }

    @NonNull
    @Override
    public AdapterCities.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_cities, parent, false);
        return new AdapterCities.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCities.ViewHolder holder, int position) {

        holder.txt_city.setText(citiesArrayList.get(position).getName());
        try{
                Picasso.get().load(citiesArrayList.get(position).getMedias().get(0).getFullsize()).into(holder.img_city);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return citiesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView img_city;
        TextView txt_city;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_city = itemView.findViewById(R.id.img_trip);
            txt_city = itemView.findViewById(R.id.txt_trip);
        }
    }
}
