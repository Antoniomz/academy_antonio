package es.rudo.android.chatripp.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.api.Pager;
import es.rudo.android.chatripp.entities.Profile;
import es.rudo.android.chatripp.utils.AdapterRecycler;

public class FriendsFragment extends Fragment {

    List<Profile> friendList = new ArrayList<>();
    View v;
    RecyclerView recyclerView;
    AdapterRecycler adapter;
    String user = "";
    int page = 1;

    public FriendsFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_friends, null);
        recyclerView = v.findViewById(R.id.recycler_friends);

        //Catch the id of user
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                Profile profile = (Profile) object;
                user = profile.getId();
                getData(user, page);
            }
        });
        return v;
    }

    public void getData(String user, int page){

        new DataWebService().getFriends(user, page, new DataStrategy.InteractDispatcherPager<Profile>() {
            @Override
            public void response(int code, Pager<Profile> pager) {
                friendList = pager.getResults();
                adapter = new AdapterRecycler(friendList);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            }
        });

    }
}