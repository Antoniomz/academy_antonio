package es.rudo.android.chatripp.api;

import android.util.Log;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import es.rudo.android.chatripp.App;
import es.rudo.android.chatripp.BuildConfig;
import es.rudo.android.chatripp.entities.Cities;
import es.rudo.android.chatripp.entities.Login;
import es.rudo.android.chatripp.entities.Posts;
import es.rudo.android.chatripp.entities.Profile;
import es.rudo.android.chatripp.entities.Trips;
import es.rudo.android.chatripp.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DataWebService extends DataStrategy{

    private Retrofit retrofit;
    private Retrofit retrofitLogin;
    private ApiService apiService;
    private ApiService apiServiceLogin;
    private String TAG_FAILURE = "FAILURE_CALL";

    public DataWebService(){

    OkHttpClient clientWithoutAuth = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .log(Platform.INFO)
            .request("Request")
            .response("Response")
            .build())
            .build();

    OkHttpClient clientWithAuth = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BODY)
                    .log(Platform.INFO)
                    .request("Request")
                    .response("Response")
                    .build())
            .addInterceptor(new AccessTokenInterceptor())
            .authenticator(new AccessTokenAuthenticator())
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientWithAuth)
                .build();

        apiService = retrofit.create(ApiService.class);

        retrofitLogin = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(clientWithoutAuth)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiServiceLogin = retrofitLogin.create(ApiService.class);

    }

    @Override
    public Call<Token> refreshToken() {
        ApiService apiServiceRefresh = retrofitLogin.create(ApiService.class);

        Login login = new Login();
        login.setRefresh_token(App.preferences.getRefreshToken());
        login.setGrant_type(Config.GRANT_TYPE);
        return apiServiceRefresh.refreshToken(login);
    }

    @Override
    public void getFirebaseToken(InteractDispatcherGeneric interactDispatcherGeneric) {
        apiService.getFirebaseToken().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                interactDispatcherGeneric.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                interactDispatcherGeneric.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get firebase token - " + t.getMessage());
            }
        });

    }

    @Override
    public void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject) {
        apiServiceLogin.postRegister(profile).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post register - " + t.getMessage());
            }
        });
    }

    @Override
    public void postLogin(Login login, InteractDispatcherObject interactDispatcherObject) {
        apiServiceLogin.postLogin(login).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post login - " + t.getMessage());
            }
        });
    }

    @Override
    public void getMe(InteractDispatcherObject interactDispatcherObject) {
        apiService.getMe().enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get profile - " + t.getMessage());
            }
        });

    }

    @Override
    public void getPosts(InteractDispatcherObject interactDispatcherObject) {
        apiService.getPosts().enqueue(new Callback<Posts>() {
            @Override
            public void onResponse(Call<Posts> call, Response<Posts> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Posts> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get posts - " + t.getMessage());
            }
        });
    }

    @Override
    public void getTrips(InteractDispatcherPager interactDispatcherPager) {
        apiService.getTrips().enqueue(new Callback<Pager<Trips>>() {
            @Override
            public void onResponse(Call<Pager<Trips>> call, Response<Pager<Trips>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Trips>> call, Throwable t) {
                interactDispatcherPager.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get trips - " + t.getMessage());
            }
        });
    }

    public void getCities(int page, InteractDispatcherPager interactDispatcherPager){
        apiService.getCities(page).enqueue(new Callback<Pager<Cities>>() {
            @Override
            public void onResponse(Call<Pager<Cities>> call, Response<Pager<Cities>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Cities>> call, Throwable t) {
                interactDispatcherPager.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get cities - " + t.getMessage());
            }
        });
    }

    public void getFriends(String user, int page, InteractDispatcherPager interactDispatcherPager){
        apiService.getFriends(user, page).enqueue(new Callback<Pager<Profile>>() {
            @Override
            public void onResponse(Call<Pager<Profile>> call, Response<Pager<Profile>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Profile>> call, Throwable t) {
                interactDispatcherPager.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get friends - " + t.getMessage());
            }
        });
    }
}
