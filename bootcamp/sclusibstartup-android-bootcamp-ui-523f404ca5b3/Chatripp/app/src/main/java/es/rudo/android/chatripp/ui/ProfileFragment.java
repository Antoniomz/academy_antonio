package es.rudo.android.chatripp.ui;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.api.Pager;
import es.rudo.android.chatripp.entities.Posts;
import es.rudo.android.chatripp.entities.Profile;
import es.rudo.android.chatripp.utils.TabAdapter;

public class ProfileFragment extends Fragment {

    ImageView img_country, img_profile;
    AppBarLayout appbar;
    FloatingActionButton fab_Settings;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsed;
    TabLayout tab;
    ViewPager viewPager;
    TextView description;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        appbar = view.findViewById(R.id.appbar_global);
        fab_Settings = view.findViewById(R.id.btn_settings_collapsed);
        toolbar = view.findViewById(R.id.toolbar_name);
        collapsed = view.findViewById(R.id.collapsing_bar);
        viewPager = view.findViewById(R.id.fragment_container);
        tab = view.findViewById(R.id.tab_layout);
        img_country = view.findViewById(R.id.img_country);
        img_profile = view.findViewById(R.id.image_profile);
        description = view.findViewById(R.id.text_description);

        setupViewPager(viewPager);

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()){
                    fab_Settings.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                    collapsed.setCollapsedTitleTextColor(Color.BLACK);
                }else if (verticalOffset == 0){
                    fab_Settings.setVisibility(View.INVISIBLE);
                    toolbar.setVisibility(View.INVISIBLE);
                }else{
                    fab_Settings.setVisibility(View.INVISIBLE);
                    toolbar.setVisibility(View.INVISIBLE);
                }
            }
        });
        loggedUser();
        getPost();
        getTrips();
    }

    //Set custom tab to tablayout
    private void customTab(TabLayout tab, String count, String name,int posicion){
        LinearLayout tabcustom = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        TextView textName = tabcustom.findViewById(R.id.tab_posts);
        TextView textCount = tabcustom.findViewById(R.id.tab_count);
        textName.setText(name);
        textCount.setText(count);
        tab.getTabAt(posicion).setCustomView(tabcustom);
    }

    private void setupViewPager(ViewPager view){
        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new PostsFragment(), "Posts");
        adapter.addFragment(new TripsFragment(), "Trips");
        adapter.addFragment(new FriendsFragment(), "Friends");
        tab.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }

    public void loggedUser(){
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {     //Add information to profile collapsed
                Profile profile = (Profile) object;
                collapsed.setTitle(profile.getFirst_name() + " " + profile.getLast_name());
                description.setText(profile.getBio());
                Picasso.get().load(profile.getImage_medias().getFile()).into(img_profile);

                //add lines to tablayout
                try {
                    customTab(tab, profile.getFriends() , "FRIENDS", 2);
                }catch (NullPointerException e){ }
            }
        });
    }

    public void getPost(){
        new DataWebService().getPosts(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                Posts posts = (Posts) object;
                try {
                    customTab(tab, posts.getCount() , "POSTS", 0);
                }catch (NullPointerException e){ }

            }
        });
    }

    public void getTrips(){
        new DataWebService().getTrips(new DataStrategy.InteractDispatcherPager() {
            @Override
            public void response(int code, Pager pager) {
                try {
                    customTab(tab, String.valueOf(Integer.valueOf(pager.getCount())), "TRIPS", 1);
                }catch (NullPointerException e){ }
            }
        });
    }
}