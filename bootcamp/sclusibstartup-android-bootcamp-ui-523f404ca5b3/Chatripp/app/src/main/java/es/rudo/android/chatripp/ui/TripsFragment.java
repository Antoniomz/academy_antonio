package es.rudo.android.chatripp.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.api.Pager;
import es.rudo.android.chatripp.entities.Media;
import es.rudo.android.chatripp.entities.Trips;
import es.rudo.android.chatripp.utils.AdapterTrips;

public class TripsFragment extends Fragment {

    List<Trips> trips = new ArrayList<>();
    View v;
    RecyclerView recyclerView;
    AdapterTrips adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_trips, container, false);

        recyclerView = v.findViewById(R.id.recycler_trips);

        new DataWebService().getTrips(new DataStrategy.InteractDispatcherPager<Trips>() {
            @Override
            public void response(int code, Pager<Trips> pager) {
                trips = pager.getResults();
                adapter = new AdapterTrips(trips);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            }
        });
        return v;
    }
}