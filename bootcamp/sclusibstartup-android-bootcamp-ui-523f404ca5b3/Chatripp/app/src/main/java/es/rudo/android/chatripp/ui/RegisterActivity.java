package es.rudo.android.chatripp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

import es.rudo.android.chatripp.R;
import es.rudo.android.chatripp.api.DataStrategy;
import es.rudo.android.chatripp.api.DataWebService;
import es.rudo.android.chatripp.entities.Profile;
import es.rudo.android.chatripp.utils.Constants;

public class RegisterActivity extends AppCompatActivity {

    Button register;
    private EditText name, email, password, confirm;
    TextInputLayout in_username, in_email, in_password, in_confirmpassword;
    CheckBox terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

    //edittext
    name = findViewById(R.id.edit_username);
    email = findViewById(R.id.edit_email);
    password = findViewById(R.id.edit_password);
    confirm = findViewById(R.id.edit_confirmpassword);
    terms = findViewById(R.id.check_terms);

    //inputslayouts
    in_username = findViewById(R.id.input_username);
    in_email = findViewById(R.id.input_email);
    in_password = findViewById(R.id.input_password);
    in_confirmpassword = findViewById(R.id.input_confirmpassword);

    register = findViewById(R.id.btn_register);

    initListeners();
}

    private void initListeners() {
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (send()) callRegister();
            }
        });
    }

    private boolean validarEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public boolean send(){
        boolean register_correct = true;
        boolean bo_username = false;
        boolean bo_email = false;
        boolean bo_password = false;
        boolean bo_confirmpassword = false;
        boolean bo_terms = false;

        bo_username = username(bo_username);
        bo_email = email(bo_email);
        bo_password = validate(bo_password);
        bo_confirmpassword = confirmpass(bo_confirmpassword);
        bo_terms = terms(bo_terms);

        if (bo_username && bo_email && bo_password && bo_confirmpassword && bo_terms){
            Toast toast = Toast.makeText(getApplicationContext(), "Todo es correcto", Toast.LENGTH_SHORT);
            toast.show();
        }
        return register_correct;
    }

    private boolean terms(boolean bo_terms) {
        if(!terms.isChecked()){
            terms.setError("La casilla debe estar marcada");
        }else{
            terms.setError(null);
            bo_terms = true;
        }
        return bo_terms;
    }

    private boolean confirmpass(boolean bo_confirmpassword) {
        if(confirm.getText().toString().isEmpty()){
            in_confirmpassword.setError("Campo vacío");
        }else if(!confirm.getText().toString().equals(password.getText().toString())){
            in_confirmpassword.setError("Las contraseñas no coinciden");
        }else{
            in_confirmpassword.setError(null);
            bo_confirmpassword = true;
        }
        return bo_confirmpassword;
    }

    private boolean validate(boolean bo_password) {
        if (password.getText().toString().isEmpty()){
            in_password.setError("Campo vacío");
        }else if (!Constants.PASSWORD_PATTERN.matcher(password.getText().toString()).matches()){
            in_password.setError("Contraseña no válida");
        }else{
            in_password.setError(null);
            bo_password = true;
        }
        return bo_password;
    }

    private boolean email(boolean bo_email) {
        if(email.getText().toString().isEmpty()){
            in_email.setError("Campo vacío");
        }else if(!validarEmail(email.getText().toString())){
            in_email.setError("Email no válido");
        }else{
            in_email.setError(null);
            bo_email = true;
        }
        return bo_email;
    }

    private boolean username(boolean bo_username) {
        if(name.getText().toString().isEmpty()){
            in_username.setError("Campo vacío");
        }else if(name.length()<2){
            in_username.setError("Mínimo 2 carácteres");
        }else{
            in_username.setError(null);
            bo_username = true;
        }
        return bo_username;
    }

    private void callRegister(){
        Profile profile = new Profile();
        profile.setUsername(name.getText().toString());
        profile.setEmail(email.getText().toString());
        profile.setPassword(password.getText().toString());
        profile.setPassword_confirm(confirm.getText().toString());

        new DataWebService().postRegister(profile, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_CREATED_CODE) {
                    goToMainactivity();
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Register", Toast.LENGTH_LONG).show();
                    register.setEnabled(true);
                }
            }
        });
    }

    private void goToMainactivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}