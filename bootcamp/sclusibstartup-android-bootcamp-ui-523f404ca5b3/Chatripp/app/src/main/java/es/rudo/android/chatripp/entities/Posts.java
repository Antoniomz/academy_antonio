package es.rudo.android.chatripp.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class Posts implements Serializable {

    private String count;
    private String next;
    private String previous;
    private ArrayList<Posts> results;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public ArrayList<Posts> getPosts() {
        return results;
    }

    public void setPosts(ArrayList<Posts> results) {
        this.results = results;
    }
}
