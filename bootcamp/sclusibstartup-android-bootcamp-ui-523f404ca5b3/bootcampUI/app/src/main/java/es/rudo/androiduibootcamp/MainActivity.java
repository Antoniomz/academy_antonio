package es.rudo.androiduibootcamp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import es.rudo.androiduibootcamp.Exercise06.Exercise06Activity;
import es.rudo.androiduibootcamp.Exercise07.Exercise07Activity;
import es.rudo.androiduibootcamp.exercise01.Exercise01Activity;
import es.rudo.androiduibootcamp.exercise02.Excercise02Activity;
import es.rudo.androiduibootcamp.exercise03.Exercise03Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_1Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_2Activity;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class MainActivity extends AppCompatActivity {

    Button btn_exercise01, btn_exercise02, btn_exercise03, btn_exercise04, btn_exercise05, btn_exercise06, btn_exercise07;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_exercise01 = findViewById(R.id. btn_exercise01);
        btn_exercise02 = findViewById(R.id. btn_exercise02);
        btn_exercise03 = findViewById(R.id. btn_exercise03);
        btn_exercise04 = findViewById(R.id. btn_exercise04);
        btn_exercise05 = findViewById(R.id. btn_exercise05);
        btn_exercise06 = findViewById(R.id. btn_exercise06);
        btn_exercise07 = findViewById(R.id. btn_exercise07);

    }

    public void onClickExercise01(View view){
        Intent i = new Intent(MainActivity.this, Exercise01Activity.class);
        startActivity(i);
    }

    public void onClickExercise02(View view){
        Intent i = new Intent(MainActivity.this, Excercise02Activity.class);
        startActivity(i);
    }

    public void onClickExercise03(View view){
        Intent i = new Intent(MainActivity.this, Exercise03Activity.class);
        startActivity(i);
    }

    public void onClickExercise04(View view){
        Intent i = new Intent(MainActivity.this, Exercise04_1Activity.class);
        startActivity(i);
    }

    public void onClickExercise05(View view){
        Intent i = new Intent(MainActivity.this, Exercise05Activity.class);
        startActivity(i);
    }

    public void onClickExercise06(View view){
        Intent i = new Intent(MainActivity.this, Exercise06Activity.class);
        startActivity(i);
    }

    public void onClickExercise07(View view){
        Intent i = new Intent(MainActivity.this, Exercise07Activity.class);
        startActivity(i);
    }

}