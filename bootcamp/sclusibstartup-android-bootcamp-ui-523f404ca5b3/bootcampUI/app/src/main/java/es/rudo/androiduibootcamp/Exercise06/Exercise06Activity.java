package es.rudo.androiduibootcamp.Exercise06;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;

public class Exercise06Activity extends AppCompatActivity {

    TextView text_save, check_save, switch_save;
    EditText edit_Text;
    CheckBox check_edit;
    Switch switch_edit;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String text = "text";
    public static final String check = "checkbox";
    public static final String sw = "switch";
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise06);

        check_edit = (CheckBox) findViewById(R.id.check_edit);
        switch_edit = (Switch) findViewById(R.id.switch_edit);
        edit_Text = (EditText) findViewById(R.id.edit_text);
        text_save = (TextView) findViewById(R.id.text_name);
        check_save = (TextView) findViewById(R.id.check_validate);
        switch_save = (TextView) findViewById(R.id.switch_validate);
        save = (Button) findViewById(R.id.btn_save);
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);

        if(sharedpreferences.contains(text)){
            text_save.setText(sharedpreferences.getString(text, ""));
        }

        Get();
    }

        public void Save(View view) {
        String n = edit_Text.getText().toString();
        boolean c = check_edit.isChecked();
        boolean s = switch_edit.isChecked();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(text, n);
        editor.putBoolean(check, c);
        editor.putBoolean(sw, s);
        editor.commit();

        Get();
        }

    public void clear(View view) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("text");
        editor.remove("checkbox");
        editor.remove("switch");
        editor.commit();

        Get();
    }

    public void Get() {
        text_save = (TextView) findViewById(R.id.text_name);
        check_save = (TextView) findViewById(R.id.check_validate);
        switch_save = (TextView) findViewById(R.id.switch_validate);
        check_edit = (CheckBox) findViewById(R.id.check_edit);
        switch_edit = (Switch) findViewById(R.id.switch_edit);
        edit_Text = (EditText) findViewById(R.id.edit_text);
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);

        if (sharedpreferences.contains(text)) {
            text_save.setText(sharedpreferences.getString(text, ""));
        }

        if (sharedpreferences.contains(check)) {
            String getBooleancheck = String.valueOf(sharedpreferences.getBoolean(check, false));
            check_save.setText(getBooleancheck);
        }

        if (sharedpreferences.contains(sw)) {
            String getBooleansw = String.valueOf(sharedpreferences.getBoolean(sw, false));
            switch_save.setText(getBooleansw);
        }

        if (sharedpreferences.contains(text)) {
            edit_Text.setText(sharedpreferences.getString(text, ""));
        }

        if (sharedpreferences.contains(check)) {
            check_edit.setChecked(sharedpreferences.getBoolean(check, false));
        }

        if (sharedpreferences.contains(sw)) {
            switch_edit.setChecked(sharedpreferences.getBoolean(sw, false));
        }
    }

}

