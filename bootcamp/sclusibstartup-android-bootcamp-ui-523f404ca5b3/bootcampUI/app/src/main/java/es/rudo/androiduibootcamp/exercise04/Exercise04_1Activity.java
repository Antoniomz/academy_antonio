package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import es.rudo.androiduibootcamp.R;

public class Exercise04_1Activity extends AppCompatActivity {

    private EditText name;
    Button button;
    static  final String send_name = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_1);

        name = (EditText) findViewById(R.id.edit_name);
        button = (Button) findViewById(R.id.btn_continue);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Exercise04_1Activity.this, Exercise04_2Activity.class);
                i.putExtra(send_name, name.getText().toString());
                startActivity(i);
            }
        });
    }
}
