package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;

public class Exercise04_2Activity extends AppCompatActivity {

    TextView text;
    EditText surname;
    Button surname_button;
    String name;
    static  final String receive_name = "name";
    static  final String send_surname = "surname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_2);
        text = (TextView) findViewById(R.id.text_name);
        surname = (EditText) findViewById(R.id.edit_surname);
        surname_button = (Button) findViewById(R.id.btn_surname);
        name = getIntent().getExtras().getString(receive_name);
        text.setText(name);

        surname_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Exercise04_2Activity.this, Exercise04_3Activity.class);
                i.putExtra(receive_name, name);
                i.putExtra(send_surname, surname.getText().toString());
                startActivity(i);
            }
        });
    }
}
