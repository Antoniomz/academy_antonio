package es.rudo.androiduibootcamp.adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Adapter extends RecyclerView.Adapter<Exercise05Adapter.ViewHolderExercise05>{

    ArrayList<User> users;
    boolean press;

    public Exercise05Adapter(ArrayList<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public ViewHolderExercise05 onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_user, null, false);
        return new ViewHolderExercise05(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderExercise05 holder, int i) {
        holder.name.setText(users.get(i).getName());
        holder.photo.setImageResource(users.get(i).getPhoto());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolderExercise05 extends RecyclerView.ViewHolder {

        TextView name;
        ImageView photo;
        Button follow;

        public ViewHolderExercise05(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.text_username);
            photo = (ImageView) itemView.findViewById(R.id.image_user);
            follow = (Button) itemView.findViewById(R.id.btn_follow);

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(follow.getText().equals("seguir")){
                        follow.setBackgroundColor(Color.GRAY);
                        follow.setText("seguido");
                    }else{
                        follow.setBackgroundResource(R.drawable.button_selected);
                        follow.setText("seguir");
                    }
                }
            });
        }
    }
}
