package es.rudo.androiduibootcamp.exercise03;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

import es.rudo.androiduibootcamp.R;

public class Exercise03Activity extends AppCompatActivity {

    private EditText name, email, password, confirm;
    TextInputLayout in_username, in_email, in_password, in_confirmpassword;
    CheckBox terms;
    final Pattern PASSWORD_PATTERN = Pattern.compile("^" + "(?=.*[0-9])" + "(?=.*[a-z])" + "(?=.*[A-Z])" + ".{8,20}" + "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise03);

        //edittext
        name = findViewById(R.id.edit_username);
        email = findViewById(R.id.edit_email);
        password = findViewById(R.id.edit_password);
        confirm = findViewById(R.id.edit_confirmpassword);
        terms = findViewById(R.id.check_terms);

        //inputslayouts
        in_username = findViewById(R.id.input_username);
        in_email = findViewById(R.id.input_email);
        in_password = findViewById(R.id.input_password);
        in_confirmpassword = findViewById(R.id.input_confirmpassword);
    }

    private boolean validarEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void send(View v){

        //validate username
        if(name.getText().toString().isEmpty()){
            in_username.setError("Campo vacío");
        }else if(name.length()<2){
                in_username.setError("Mínimo 2 carácteres");
            }else
                in_username.setError(null);

        //validate email
        if(email.getText().toString().isEmpty()){
            in_email.setError("Campo vacío");
        }else if(!validarEmail(email.getText().toString())){
                in_email.setError("Email no válido");
            }else
                in_email.setError(null);

        //validate password
        if (password.getText().toString().isEmpty()){
            in_password.setError("Campo vacío");
        }else if (!PASSWORD_PATTERN.matcher(password.getText().toString()).matches()){
            in_password.setError("Contraseña no válida");
        }else
            in_password.setError(null);

        //validate confirmpassword
        if(confirm.getText().toString().isEmpty()){
            in_confirmpassword.setError("Campo vacío");
        }else if(!confirm.getText().toString().equals(password.getText().toString())){
                in_confirmpassword.setError("Las contraseñas no coinciden");
            }else
                in_confirmpassword.setError(null);

        //validate check terms
        if(!terms.isChecked()){
            terms.setError("La casilla debe estar marcada");
        }else
            terms.setError(null);

    }
}
