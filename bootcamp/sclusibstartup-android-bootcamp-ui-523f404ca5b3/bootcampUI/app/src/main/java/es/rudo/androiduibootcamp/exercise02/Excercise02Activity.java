package es.rudo.androiduibootcamp.exercise02;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Excercise02Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise02);

        final Button button1 = (Button) findViewById(R.id.btn_login);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Iniciar sesión", Toast.LENGTH_SHORT).show();
            }
        });

        final TextView text1 = (TextView) findViewById(R.id.text_forget_password);
        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Recordar contraseña", Toast.LENGTH_SHORT).show();
            }
        });

        final TextView text2 = (TextView) findViewById(R.id.text_create_account);
        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Crear Contraseña", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
