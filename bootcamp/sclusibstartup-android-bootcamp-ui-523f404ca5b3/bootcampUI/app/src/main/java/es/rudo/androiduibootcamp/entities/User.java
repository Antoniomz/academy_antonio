package es.rudo.androiduibootcamp.entities;

import android.widget.ImageView;

public class User {

    private String name;
    private int photo;

    public User(String name, int photo){
        this.name = name;
        this.photo = photo;
    }

    public User(String carlos, Integer o){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
