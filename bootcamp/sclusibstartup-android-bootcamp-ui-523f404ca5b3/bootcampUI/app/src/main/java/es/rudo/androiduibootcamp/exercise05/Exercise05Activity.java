package es.rudo.androiduibootcamp.exercise05;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Button;

import java.util.ArrayList;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.adapters.Exercise05Adapter;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Activity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    ArrayList<User> users;
    RecyclerView recyclerUsers;
    Button follow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise05);
        follow = (Button) findViewById(R.id.btn_follow);

        users = new ArrayList<>();
        recyclerUsers = (RecyclerView) findViewById(R.id.recycler_users);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(this));

        addusers();

        Exercise05Adapter adapter = new Exercise05Adapter(users);
        recyclerUsers.setAdapter(adapter);
    }

    private void addusers(){
        users.add(new User("Antonio", R.drawable.equipo1));
        users.add(new User("Julia", R.drawable.equipo2));
        users.add(new User("Alberto", R.drawable.ic_baseline_person_24));
        users.add(new User("Carlos", R.drawable.ic_baseline_person_24));
        users.add(new User("fdosalo@gmail.com", R.drawable.ic_baseline_person_24));
        users.add(new User("Sara", R.drawable.equipo3));
        users.add(new User("Jose", R.drawable.ic_baseline_person_24));
        users.add(new User("Manuel", R.drawable.equipo1));
        users.add(new User("lau@gmail.com", R.drawable.equipo3));
        users.add(new User("Marta", R.drawable.equipo2));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.action_search:
                fragment = new SearchedFragment();
                break;
        }

        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragments_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}

