package es.rudo.androiduibootcamp.exercise04;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;

public class Exercise04_3Activity extends AppCompatActivity {

    TextView fullname;
    Button exit;
    static  final String receive_name = "name";
    static  final String receive_surname = "surname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_3);
        fullname = (TextView) findViewById(R.id.text_fullname);
        exit = (Button) findViewById(R.id.btn_exit);
        String name = getIntent().getStringExtra(receive_name);
        String surname = getIntent().getStringExtra(receive_surname);

        fullname.setText(name + " " + surname);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
    }
}
