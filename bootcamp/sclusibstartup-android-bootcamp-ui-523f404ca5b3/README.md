![rudo logo](https://rudo.es/bootcamp/rudo.png)

**Bienvenidos a BOOTCAMP UI**

Lo primero y más importante es que una vez os hayáis bajado este repositorio os creéis una rama con vuestro nombre. A partir de ahora tendréis que hacer commit de vuestros avances en esa rama.

El objetivo de estas pruebas es que aprendáis a realizar la maquetación y funcionalidad típica de un proyecto Android por medio de Android Studio. Primero nos centraremos en maquetación, luego en funcionalidad y por último llamadas a servidor.

Para realizar cada ejercicio acude al archivo AndroidManifest.xml y coloca el intent-filter MAIN y LAUNCHER en el ejercicio a realizar. La maquetación se realizará en la carpeta res/layout y la funcionalidad en NombreDeEjercicioActiviy o Fragments según el tipo de estructura. Se valorará un código limpio y bien estructurado tanto en el xml del diseño como en el código escrito.

*Recordar cualquier duda que os surja acudir a vuestro Sensei, su función es de guia no de solucionador. Vosotros sois los que debéis acabarlo*

#Indice

1. ✏️[ EXERCISES ](#markdown-header-exercises)
    *  [ EXERCISE.01 ](#markdown-header-exercise01)
	*  [ EXERCISE.02 ](#markdown-header-exercise02)
	*  [ EXERCISE.03 ](#markdown-header-exercise03)
	*  [ EXERCISE.04 ](#markdown-header-exercise04)
	*  [ EXERCISE.05 ](#markdown-header-exercise05)
	*  [ EXERCISE.06 ](#markdown-header-exercise06)
	*  [ EXERCISE.07 ](#markdown-header-exercise07)
2. 🤖[ Code ](#markdown-header-code)
3. ☎️[ Endpoints ](#markdown-header-endpoints)
4. 📱[ App ](#markdown-header-app)


#EXERCISES

✏️ A través de estes ejercicios aprenderás a diseñar una interfaz de usuario completa. La formación abarca desde posicionar objetos en la vista hasta añadir componentes más complicados como RecyclerView y navegación. También más adelante aprenderás a realizar varias funcionalidades que servirán para hacer cualquier app.

##EXERCISE.01

En esta primera prueba tendréis que hacer que el logo de rudo se alinee en el centro de la pantalla. Además debajo de éste añadir dos textos alineados al centro de éste.

![Ejemplo 1](https://rudo.es/bootcamp/exercise01.jpg = 250x)

1. Crea un ImageView y dos TextView, además de los layout que necesites para replicar el contenedor de la imagen.
2. Importa los assets (los encontrarás en la carpeta images del repositorio).
3. Coloca la imagen y los dos TextView con la estructura más óptima para replicar el diseño.
4. Testea la aplicación y comprueba que se ve igual que en el ejemplo mostrado. 
5. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.02

Después de un primer ejercicio sencillo vamos a realizar la maquetación de una pantalla de login entera.

![Ejemplo 2](https://rudo.es/bootcamp/exercise02.jpg = 250x)

1. Crea una estructura base LinearLayout con orientación vertical.
2. Coloca los elementos necesarios (EditText, TextView, etc) para montar la estructura.
3. Cada botón tendrá que mostrar un Toast con la acción que realiza (Iniciar sesión, Recordar contraseña y Crear contraseña).
4. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.03

¡Ahora es hora de ponerse en acción! Vamos a proceder a realizar la funcionalidad de un registro completo.

![Ejemplo 3](https://rudo.es/bootcamp/exercise03.png = 250x)

1. A partir de la vista proporcionada, añade la siguiente funcionalidad en el Activity. No te confíes ya que no te lo daremos absolutamente todo montado ;)
2. Indica en pantalla los errores correspondientes a los campos que sean incorrectos. Las comprobaciones se indicarán en el siguiente punto.
3. Comprueba que ningún campo sea vacío. El nombre debe contener almenos 2 carácteres. El email tendrá que tener un formato válido. La contraseña debe tener almenos 8 carácteres, 1 letra mayúscula, 1 letra minúsucula y 1 número, además la confirmación de la contraseña debe ser igual que la contraseña. El Checkbox de términos y condiciones deberá estar marcado.
4. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.04

Vamos ahora con un ejercicio de navegación. El objetivo será pasar información entre activities y mostrar en la última el resultado. Para ello dispondrás de tres actividades:

1. Primera Actividad: Se encarga de recoger el nombre del usuario a través de un Editext y al pulsar continuar pasar ese String a la siguiente actividad.
2. Segunda Actividad: Tiene que recoger el String del nombre que le ha mandado la primera actividad, mostrarlo en el textview y solicitar los apellidos. Al hacer clic en continuar concatenará el nombre con los apellidos y se lo mandará a la tercera y última actividad.
3. Tercera Actividad: La última actividad tiene que recoger el String enviado por la segunda actividad y mostrarlo. Además ésta actividad si pulsas atrás debe de cerrarse toda la aplicación por completo y no volver a la anterior actividad.
4. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.05

Ahora vamos a proceder con listados. El objetivo será hacer un ArrayList de objetos tipo User y rellenarla en un recyclerview.

![Ejemplo 5](https://rudo.es/bootcamp/exercise05.png = 250x)

1. Crea la entidad User con las propiedades que veas oportunas para mostrar la información requerida en la imagen de ejemplo.
2. Haz la funcionalidad del adapter para mostrar cada elemento en el listado. El diseño a utilizar es el siguiente: item_search_user.xml
3. Crea el listado de 10 usuarios y pintalos sobre el listado.
4. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.06

En este ejercicio practicaremos cómo guardar datos en la caché, cómo recuperarlos y usarlos para mostrar la pantalla como la guardamos.

1. Acaba el layout poniendo los ids de los elementos que vas a utilizar.
2. Al pulsar en guardar hay que almacenar en caché por medio de sharedpreferences el texto del edittex, si está o no activado el checkbox y el estado del swtich.
3. Al cerrar y abrir completamente la aplicación debe cargar desde caché las variables guardadas y setear los elementos tal y cómo los tenías al cerrar la app.
4. Al pulsar el botón borrar tienes que purgar completamente la caché y dejar los elementos a su estado original.
4. Al acabar haz un commit de tu trabajo.

---

##EXERCISE.07

Ahora vayamos con algo más complicado. Hay que montar un menú inferior(bottombar) con cuatro botones. Cada botón abrirá el fragment correspondiente.

1. Crea el layout usando un frameLayout para poner los fragments y un bottombar para el menú.
2. Genera el menú con los iconos(puedes usar los vectors que vienen en android Studio) y textos siguientes: Home, Search, Add y profile.
3. Crear 4 fragments cada uno con el número en el centro mostrando el fragment que es.
3. Crear la funcionalidad de al pulsar cada botón del bottombar te lleve al fragment correspondiente.
4. Al acabar haz un commit de tu trabajo.

---


#CODE

🤖

```java
    WORK IN PROGRESS...
```

#ENDPOINTS

☎️ 

```
This is a code block
```

#APP 

📱

WORK IN PROGRESS...

DONE:
- Diseño de una pantalla con una imagen y dos textos centrados debajo de esta.
- Maquetar un login.
- Funcionalidad de validaciones de campos con un diseño de registro dado. 
Extra: Botón de registro con un valor random entre 0-1000. Si es par, ha pasado el registro.
- Realizar 3 pantallas que se relacionen.
	Primera actividad: Introducir nombre.
	Pasa a la segunda actividad: Introducir apellidos.
	Pasa a la última actividad: Mostrar nombre y apellidos.
	Si pulsa atrás en la última actividad: salir de la app.
- Realizar un listado vertical con adaptador. Diseño del item del adaptador dado.
- Pasamos un diseño con componentes checkbox, switch y edittext. Guardar en preferencias el estado de los check,
switch y textos.
- Realizar un menú inferior con 4 fragments.

TODO:
- Realizar un listado por columnas. Realizar diseño del item. (Imagen desde Glide o Picasso).
- Realizar dos tabs con sus respectivos fragments.
- Realizar una lista usuarios, con imagen y nombre debajo.
Al clickar en un item muestre la información en un texto debajo.
- Realizar un login con llamada y validaciones.
- Con un login dado, realizar una pantalla en la que muestren la información del usuario con llamada /me y 
puedan editarla con un patch.
- Realizar un listado con datos de una llamada.
Extra: Llamada paginada con esa lista.