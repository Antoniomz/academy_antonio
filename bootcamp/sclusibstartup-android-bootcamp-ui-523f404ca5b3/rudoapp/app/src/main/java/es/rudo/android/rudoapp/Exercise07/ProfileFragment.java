package es.rudo.android.rudoapp.Exercise07;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import es.rudo.android.rudoapp.R;
import es.rudo.android.rudoapp.exercise04.Exercise04_2Activity;

public class ProfileFragment extends Fragment {

    //Exercise04_1 adapted

    View v;
    private EditText name;
    Button button;
    static  final String send_name = "name";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profile, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        name = getView().findViewById(R.id.edit_name);
        button = getView().findViewById(R.id.btn_continue);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), Exercise04_2Activity.class);
                i.putExtra(send_name, name.getText().toString());
                startActivity(i);
            }
        });
    }
}
