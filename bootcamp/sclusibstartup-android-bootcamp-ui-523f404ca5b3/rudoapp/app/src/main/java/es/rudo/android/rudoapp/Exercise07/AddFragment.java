package es.rudo.android.rudoapp.Exercise07;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import es.rudo.android.rudoapp.R;

public class AddFragment extends Fragment {

    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add, null);
        return v;
    }

    TextView text_save, check_save, switch_save;
    EditText edit_Text;
    CheckBox check_edit;
    Switch switch_edit;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String text = "text";
    public static final String check = "checkbox";
    public static final String sw = "switch";
    Button save, clear;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setContentView(R.layout.fragment_home);

        check_edit = getView().findViewById(R.id.check_edit);
        switch_edit = getView().findViewById(R.id.switch_edit);
        edit_Text = getView().findViewById(R.id.edit_text);
        text_save = getView().findViewById(R.id.text_name);
        check_save = getView().findViewById(R.id.check_validate);
        switch_save = getView().findViewById(R.id.switch_validate);
        save = getView().findViewById(R.id.btn_save);
        clear = getView().findViewById(R.id.btn_delete);
        sharedpreferences = getActivity().getSharedPreferences(mypreference, Context.MODE_PRIVATE);

        if(sharedpreferences.contains(text)){
            text_save.setText(sharedpreferences.getString(text, ""));
        }

        get();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = edit_Text.getText().toString();
                boolean c = check_edit.isChecked();
                boolean s = switch_edit.isChecked();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(text, n);
                editor.putBoolean(check, c);
                editor.putBoolean(sw, s);
                editor.commit();

                get();
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.remove("text");
                editor.remove("checkbox");
                editor.remove("switch");
                editor.commit();

                get();
            }
        });
    }

    private void setContentView(int fragment_home) {
    }

    public void get() {

        if (sharedpreferences.contains(text)) {
            text_save.setText(sharedpreferences.getString(text, ""));
        }

        if (sharedpreferences.contains(check)) {
            String getBooleancheck = String.valueOf(sharedpreferences.getBoolean(check, false));
            check_save.setText(getBooleancheck);
        }

        if (sharedpreferences.contains(sw)) {
            String getBooleansw = String.valueOf(sharedpreferences.getBoolean(sw, false));
            switch_save.setText(getBooleansw);
        }

        if (sharedpreferences.contains(text)) {
            edit_Text.setText(sharedpreferences.getString(text, ""));
        }

        if (sharedpreferences.contains(check)) {
            check_edit.setChecked(sharedpreferences.getBoolean(check, false));
        }

        if (sharedpreferences.contains(sw)) {
            switch_edit.setChecked(sharedpreferences.getBoolean(sw, false));
        }
    }
}