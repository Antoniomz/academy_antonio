package es.rudo.android.rudoapp.exercise03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

import es.rudo.android.rudoapp.Exercise07.Exercise07Activity;
import es.rudo.android.rudoapp.R;

public class Exercise03Activity extends AppCompatActivity {

    private EditText name, email, password, confirm;
    TextInputLayout in_username, in_email, in_password, in_confirmpassword;
    CheckBox terms;
    final Pattern PASSWORD_PATTERN = Pattern.compile("^" + "(?=.*[0-9])" + "(?=.*[a-z])" + "(?=.*[A-Z])" + ".{8,20}" + "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise03);

        //edittext
        name = findViewById(R.id.edit_username);
        email = findViewById(R.id.edit_email);
        password = findViewById(R.id.edit_password);
        confirm = findViewById(R.id.edit_confirmpassword);
        terms = findViewById(R.id.check_terms);

        //inputslayouts
        in_username = findViewById(R.id.input_username);
        in_email = findViewById(R.id.input_email);
        in_password = findViewById(R.id.input_password);
        in_confirmpassword = findViewById(R.id.input_confirmpassword);
    }

    private boolean validarEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void send(View v){

        boolean bo_username = false;
        boolean bo_email = false;
        boolean bo_password = false;
        boolean bo_confirmpassword = false;
        boolean bo_terms = false;

        //validate username
        if(name.getText().toString().isEmpty()){
            in_username.setError("Campo vacío");
        }else if(name.length()<2){
            in_username.setError("Mínimo 2 carácteres");
        }else{
            in_username.setError(null);
            bo_username = true;
        }

        //validate email
        if(email.getText().toString().isEmpty()){
            in_email.setError("Campo vacío");
        }else if(!validarEmail(email.getText().toString())){
            in_email.setError("Email no válido");
        }else{
            in_email.setError(null);
            bo_email = true;
            }


        //validate password
        if (password.getText().toString().isEmpty()){
            in_password.setError("Campo vacío");
        }else if (!PASSWORD_PATTERN.matcher(password.getText().toString()).matches()){
            in_password.setError("Contraseña no válida");
        }else{
            in_password.setError(null);
            bo_password = true;
        }

        //validate confirmpassword
        if(confirm.getText().toString().isEmpty()){
            in_confirmpassword.setError("Campo vacío");
        }else if(!confirm.getText().toString().equals(password.getText().toString())){
            in_confirmpassword.setError("Las contraseñas no coinciden");
        }else{
            in_confirmpassword.setError(null);
            bo_confirmpassword = true;
        }


        //validate check terms
        if(!terms.isChecked()){
            terms.setError("La casilla debe estar marcada");
        }else{
            terms.setError(null);
            bo_terms = true;
        }

        if (bo_username == true && bo_email == true && bo_password == true && bo_confirmpassword == true && bo_terms == true){
            Toast toast = Toast.makeText(getApplicationContext(), "Todo es correcto", Toast.LENGTH_SHORT);
            toast.show();
            Intent i = new Intent(Exercise03Activity.this, Exercise07Activity.class);
            startActivity(i);
            finish();
        }

    }

}
