package es.rudo.android.rudoapp.Exercise07;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import es.rudo.android.rudoapp.R;

public class HomeFragment extends Fragment {

    Button maps;
    View v;
    ImageView iv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        iv = view.findViewById(R.id.imgvw_rudo);
        maps = view.findViewById(R.id.btn_maps);

        //image of logo
        Picasso.get().load("https://startupv.webs.upv.es/wp-content/uploads/2018/12/logotipo_rudo_400x400.png").into(iv);

        //intent for maps
        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri rudoMaps = Uri.parse("google.streetview:cbll=39.4817896,-0.3559966,3");
                Intent mapsIntent = new Intent(Intent.ACTION_VIEW, rudoMaps);
                mapsIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapsIntent);
            }
        });
    }
}
