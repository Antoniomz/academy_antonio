package es.rudo.android.rudoapp.exercise04;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.android.rudoapp.R;

public class Exercise04_3Activity extends AppCompatActivity {

    TextView fullname;
    Button exit;
    static  final String receive_name = "name";
    static  final String receive_surname = "surname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_3);
        fullname = (TextView) findViewById(R.id.text_fullname);
        exit = (Button) findViewById(R.id.btn_exit);
        String name = getIntent().getStringExtra(receive_name);
        String surname = getIntent().getStringExtra(receive_surname);

        fullname.setText(name + " " + surname);

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle(R.string.alert_important);
        dialogo1.setMessage(R.string.alert_close);
        dialogo1.setCancelable(false);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        finishAffinity();
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Toast t=Toast.makeText(getApplicationContext(), R.string.alert_running, Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
                dialogo1.show();
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
    }
}