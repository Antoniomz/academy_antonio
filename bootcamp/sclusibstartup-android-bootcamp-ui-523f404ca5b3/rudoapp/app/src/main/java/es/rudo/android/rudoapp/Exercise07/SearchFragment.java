package es.rudo.android.rudoapp.Exercise07;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.rudo.android.rudoapp.R;
import es.rudo.android.rudoapp.adapters.Exercise05Adapter;
import es.rudo.android.rudoapp.entities.User;

public class SearchFragment extends Fragment {

    SearchView sv;
    View v;
    RecyclerView recyclerView;
    List<User> listUser;
    Exercise05Adapter adapter;

    public SearchFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_search, null);
        sv = v.findViewById(R.id.search_name);

         recyclerView = v.findViewById(R.id.rw_search);
         adapter = new Exercise05Adapter(listUser, getContext());
         recyclerView.setAdapter(adapter);
         recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

         //searchview
         sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
             @Override
             public boolean onQueryTextSubmit(String query) {
                 return false;
             }

             @Override
             public boolean onQueryTextChange(String newText) {
                 adapter.getFilter().filter(newText.toString());
                 return false;
             }
         });

         return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listUser = new ArrayList<>();
        listUser.add(new User("Antonio", R.drawable.equipo1));
        listUser.add(new User("Julia", R.drawable.equipo2));
        listUser.add(new User("Alberto", R.drawable.ic_baseline_person_24));
        listUser.add(new User("Carlos", R.drawable.ic_baseline_person_24));
        listUser.add(new User("fdosalo@gmail.com", R.drawable.ic_baseline_person_24));
        listUser.add(new User("Sara", R.drawable.equipo3));
        listUser.add(new User("Jose", R.drawable.ic_baseline_person_24));
        listUser.add(new User("Manuel", R.drawable.equipo1));
        listUser.add(new User("lau@gmail.com", R.drawable.equipo3));
        listUser.add(new User("Marta", R.drawable.equipo2));
    }

}
