package es.rudo.android.rudoapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import es.rudo.android.rudoapp.R;
import es.rudo.android.rudoapp.entities.User;

public class Exercise05Adapter extends RecyclerView.Adapter<Exercise05Adapter.ViewHolderExercise05> implements Filterable {

    List<User> users;
    List<User> allusers;
    Context context;

    public Exercise05Adapter(List<User> users, Context context) {

        this.users = users;
        this.context = context;
        this.allusers = new ArrayList<>(users);
    }

    @NonNull
    @Override
    public ViewHolderExercise05 onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_user, null, false);
        return new ViewHolderExercise05(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderExercise05 holder, int i) {
        holder.name.setText(users.get(i).getName());
        holder.photo.setImageResource(users.get(i).getPhoto());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public Filter getFilter() {
        return FilterUser;
    }

    //filter for searchview
    private Filter FilterUser = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String searchtext = charSequence.toString().toLowerCase();
            List<User> filteredList = new ArrayList<>();
            if (searchtext.length() == 0 || searchtext.isEmpty()){
                filteredList.addAll(allusers);
            }else{
                for (User item: allusers){
                    if (item.getName().toLowerCase().contains(searchtext)){
                        filteredList.add(item);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            users.clear();
            users.addAll((Collection<? extends User>) filterResults.values);
            notifyDataSetChanged();
        }
    };


    public class ViewHolderExercise05 extends RecyclerView.ViewHolder {

        TextView name;
        ImageView photo;
        Button follow;

        public ViewHolderExercise05(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.text_username);
            photo = itemView.findViewById(R.id.image_user);
            follow = itemView.findViewById(R.id.btn_follow);

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(follow.getText().equals("seguir")){
                        follow.setBackgroundColor(Color.GRAY);
                        follow.setText("seguido");
                    }else{
                        follow.setBackgroundResource(R.drawable.button_selected);
                        follow.setText("seguir");
                    }
                }
            });
        }
    }
}
