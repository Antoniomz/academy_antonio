package es.rudo.android.rudoapp.exercise02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import es.rudo.android.rudoapp.Exercise07.Exercise07Activity;
import es.rudo.android.rudoapp.R;
import es.rudo.android.rudoapp.exercise03.Exercise03Activity;

public class Exercise02Activity extends AppCompatActivity {

    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise02);

        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);

        final Button button1 = (Button) findViewById(R.id.btn_login);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!username.getText().toString().isEmpty() && !password.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Iniciar sesión", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Exercise02Activity.this, Exercise07Activity.class);
                    startActivity(i);
                }
            }
        });

        final TextView text1 = (TextView) findViewById(R.id.text_forget_password);
        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Recordar contraseña", Toast.LENGTH_SHORT).show();
            }
        });

        final TextView text2 = (TextView) findViewById(R.id.text_create_account);
        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Exercise02Activity.this, Exercise03Activity.class);
                startActivity(i);
            }
        });

    }

}