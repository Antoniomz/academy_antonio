package es.rudo.android.rudoapp.exercise01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import es.rudo.android.rudoapp.R;
import es.rudo.android.rudoapp.exercise02.Exercise02Activity;

public class Exercise01Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise01);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Exercise01Activity.this, Exercise02Activity.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }
}