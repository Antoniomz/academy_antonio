package es.rudo.android.rudoapp.Exercise07;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import es.rudo.android.rudoapp.R;

public class MapsFragment extends Fragment {

    GoogleMap gMap;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {
            gMap = googleMap;
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());


            LatLng spain = new LatLng(39, 0);
            googleMap.addMarker(new MarkerOptions().position(spain).title("Marker in Spain"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(spain));

            gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    String strAdd = "";
                    //Create Marker
                    MarkerOptions markerOptions = new MarkerOptions();

                    //Set marker position
                    markerOptions.position(latLng);

                    try {
                        List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                        if (addresses != null){
                            Address returnAddress = addresses.get(0);
                            StringBuilder stringBuilder = new StringBuilder("");
                            for (int i = 0; i <= returnAddress.getMaxAddressLineIndex(); i++) {
                                stringBuilder.append(returnAddress.getAddressLine(i)).append("\n");
                                strAdd = stringBuilder.toString();
                                Toast toast = Toast.makeText(getContext(), strAdd, Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Set latitude and longitude
                    markerOptions.title("Latitude: " + latLng.latitude + " Longitude: " + latLng.longitude);
                    Toast toast = Toast.makeText(getContext(), "Latitude: " +latLng.latitude + " Longitude: " + latLng.longitude , Toast.LENGTH_SHORT);
                    toast.show();

                    //Clear other mark
                    gMap.clear();

                    //Ad marker on map
                    gMap.addMarker(markerOptions);
                }
            });
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}