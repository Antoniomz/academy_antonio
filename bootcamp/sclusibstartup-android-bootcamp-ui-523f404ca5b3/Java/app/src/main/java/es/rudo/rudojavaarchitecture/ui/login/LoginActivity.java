package es.rudo.rudojavaarchitecture.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.rudo.rudojavaarchitecture.MainActivity;
import es.rudo.rudojavaarchitecture.R;
import es.rudo.rudojavaarchitecture.api.Config;
import es.rudo.rudojavaarchitecture.api.DataStrategy;
import es.rudo.rudojavaarchitecture.api.DataWebService;
import es.rudo.rudojavaarchitecture.entities.Login;
import es.rudo.rudojavaarchitecture.utils.Constants;
import es.rudo.rudojavaarchitecture.utils.Utils;

public class LoginActivity extends AppCompatActivity {

/*  CLASS ORDER:
    1- Constants
    2- Fields
    3- Constructors
    4- Override methods and callbacks (public or private)
    5- Public methods
    6- Private methods
    7- Inner classes or interfaces
*/

    Context context;

    @BindView(R.id.image_logo)
    ImageView imageLogo;
    @BindView(R.id.edit_username)
    EditText editUsername;
    @BindView(R.id.il_username)
    TextInputLayout ilUsername;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.text_forgot_password)
    TextView textForgotPassword;
    @BindView(R.id.text_register)
    TextView textRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = this;

        textRegister.setText(Utils.fromHtml(getString(R.string.new_register)));

        initListeners();
    }

    /**
     * Init listeners for UI elements
     */
    private void initListeners() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoginCorrect()) callLogin();
            }
        });
    }

    /**
     * Checks if username and password are correct to send
     * @return true or false
     */
    private boolean isLoginCorrect() {
        boolean isCorrect = true;

        if (editUsername.getText().toString().isEmpty() || !isEmailCorrect()) {
            isCorrect = false;
        }
        if (editPassword.getText().toString().isEmpty()) {
            isCorrect = false;
        }

        return isCorrect;
    }

    /**
     * Checks if the username pattern is correct
     * @return true or false
     */
    private boolean isEmailCorrect() {
        return editUsername.getText().toString().matches(Constants.EMAIL_PATTERN);
    }

    /**
     * Calls api to know if user is registered to the database
     */
    private void callLogin() {
        buttonLogin.setEnabled(false);

        Login login = new Login();
        login.setUsername(editUsername.getText().toString());
        login.setPassword(editPassword.getText().toString());
        login.setGrant_type(Config.GRANT_TYPE_LOGIN);

        new DataWebService().postLogin(login, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    goToMainactivity();
                } else {
                    Toast.makeText(context, "Invalid Login", Toast.LENGTH_LONG).show();
                    buttonLogin.setEnabled(true);
                }
            }
        });
    }

    /**
     * Go to the main app activity
     */
    private void goToMainactivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}